from django.db import models
from django import forms

# settings
from rest_framework.compat import MinValueValidator
from solo.models import SingletonModel


class ThirdPartServicesSettings(SingletonModel):
    # ---------------
    # --- SMSC.ru ---
    # ---------------
    sms_service_login = models.CharField(
        max_length=256,
        verbose_name='СМС Сервис логин',
        default='login'
    )
    sms_service_password = models.CharField(
        max_length=256,
        verbose_name='СМС Сервис пароль',
        default='password'
    )

    sms_service_enabled = models.BooleanField(
        default=False,
        verbose_name="Включен ли смс-сервис"
    )

    balance = models.CharField(
        default="",
        max_length=20,
        verbose_name="Баланс"
    )

    # -------------------------
    # --- GOOGLE PLACES API ---
    # -------------------------
    google_places_api_key = models.CharField(
        max_length=256,
        verbose_name='Google Places API key',
        default='AIzaSyBiJLN43xDCBrMYO11gS9X2qTdy-MsYVZU'
    )

    kilometer_constant = models.FloatField(
        default=0.05,
        verbose_name="Константа для радиуса поиска"
    )

    def __str__(self):
        return "Настройки сторонних сервисов"

    class Meta:
        verbose_name = "Настройки сторонних сервисов"


class PseudoCounter(SingletonModel):
    # -----------------------------------
    # --- Стартовое число на счетчике ---
    # -----------------------------------
    count = models.IntegerField(
        default=0,
        validators=[
            MinValueValidator(0)
        ],
        verbose_name="Начальное число на счетчике"
    )

    def __str__(self):
        return "Настройка счетчика"

    class Meta:
        verbose_name = "Настройка счетчика"


class Debuging(SingletonModel):
    # ---------------------------------------------------
    # --- Вспомогательные фичи для отладки приложения ---
    # ---------------------------------------------------

    is_debug = models.BooleanField(
        default=False,
        verbose_name="Отладка приложения"
    )

    def __str__(self):
        return "Отладка приложения"

    class Meta:
        verbose_name = "Отладка приложения"


class SiteSettings(SingletonModel):
    # --------------------------------
    # --- Настройки контента сайта ---
    # --------------------------------
    phone = models.CharField(
        default="",
        max_length=50,
        verbose_name="Телефон"
    )

    email = models.CharField(
        default="",
        max_length=200,
        verbose_name="Адрес электронной почты"
    )

    developer = models.CharField(
        default="",
        max_length=70,
        verbose_name="Ссылка на разработчика"
    )

    center_text = models.TextField(
        default="",
        verbose_name="Описание проекта"
    )

    footer_name = models.CharField(
        default="",
        max_length=200,
        verbose_name="Надпись в футере"
    )

    header_name = models.CharField(
        default="",
        max_length=200,
        verbose_name="Надпись в хедере"
    )

    class Meta:
        verbose_name = "Настройки сайта"


class Sizer(SingletonModel):
    # ---------------------------
    # --- Размеры изображений ---
    # ---------------------------

    width_profile = models.IntegerField(
        default=300,
        validators=[
            MinValueValidator(0)
        ],
        verbose_name="Ширина изображения в профиле"
    )

    height_profile = models.IntegerField(
        default=300,
        validators=[
            MinValueValidator(0)
        ],
        verbose_name="Высота изображения в профиле"
    )

    cost = models.IntegerField(
        default=0,
        validators=[
            MinValueValidator(0)
        ],
        verbose_name="Стоимость подписки (пока не работает)"
    )

    def __str__(self):
        return "Таблица размеров"

    class Meta:
        verbose_name = "Таблица размеров"
