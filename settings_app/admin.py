from django.contrib import admin
from .models import ThirdPartServicesSettings, PseudoCounter, Debuging, SiteSettings, Sizer
from solo.admin import SingletonModelAdmin
from django import forms

# Register your models here.


class SingletonModelAdminOverwrite(SingletonModelAdmin):
    # кнопки сверху
    save_on_top = True
    # readonly_fields = ['balance']

    fieldsets = [
        ('Google places', {
            'fields': ('google_places_api_key', 'kilometer_constant', )
        }),
        ('СМС сервис', {
            'fields': (('sms_service_login', 'sms_service_password'), 'sms_service_enabled', 'balance')
        })
    ]


admin.site.register(ThirdPartServicesSettings, SingletonModelAdminOverwrite)


class SingletonCounterModel(SingletonModelAdmin):

    fieldsets = [
        ('Счетчик', {
            'fields': ('count', )
        }),
    ]


admin.site.register(PseudoCounter, SingletonCounterModel)


class SingletonDebugingModel(SingletonModelAdmin):

    fieldsets = [
        ('Отладка', {
            'fields': ('is_debug', )
        }),
    ]


# admin.site.register(Debuging, SingletonDebugingModel)


class SingletonSiteModel(SingletonModelAdmin):

    fieldsets = [
        ('Шапка', {
            'fields': ('header_name', )
        }),
        ('Данные', {
            'fields': ('phone', 'email', 'developer', 'center_text')
        }),
        ('Футер', {
            'fields': ('footer_name',)
        }),
    ]


admin.site.register(SiteSettings, SingletonSiteModel)


class SingletonSizerModel(SingletonModelAdmin):
    # fields = (('width_profile', 'height_profile', ))
    fieldsets = [
        ('Профиль', {
            'fields': (('width_profile', 'height_profile', ), )
        }),
        ('Подписка', {
            'fields': ('cost', )
        }),
    ]


admin.site.register(Sizer, SingletonSizerModel)
