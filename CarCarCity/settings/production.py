from .base import *

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = False

ALLOWED_HOSTS = ['.carcar-city.ru']

PREPEND_WWW = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/
STATIC_ROOT = os.path.join(BASE_DIR, '..', 'files', 'static/')
STATIC_URL = '/static/'

# TEMPLATE_DIRS = (os.path.join(BASE_DIR,  'templates'),)

MEDIA_ROOT = os.path.join(BASE_DIR, '..', 'files', 'media/')
MEDIA_URL = '/media/'


EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_PORT = 587
EMAIL_USE_TLS = True