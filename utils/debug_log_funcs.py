import time
from typing import Tuple, Callable
import logging


STRING_LOCAL_DB_FOR_LOG = '--({1:.3f} сек.)--JSON_DB-- доступ к бд (db_name: {0}) и поиск в ней'
STRING_API_FOR_LOG = '--({0:.3f} сек.)--API-- request'
STRING_WITHOUT_API_FOR_LOG = '--WITHOUT_API--'
STRING_TIME_FOR_LOG = '--({0:.3f} сек.)--{1}'


class RunTimer:

    start_time = 0
    calculated_time = 0
    message = ''

    def __init__(self, message):
        self.start_time = time.time()
        self.message = message

    def stop(self):
        self.calculated_time = time.time() - self.start_time

    def log_result(self):
        logging.debug(STRING_TIME_FOR_LOG.format(self.calculated_time, self.message))

    def stop_and_log(self):
        self.stop()
        self.log_result()
