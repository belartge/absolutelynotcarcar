from settings_app.models import ThirdPartServicesSettings, PseudoCounter, Debuging, SiteSettings, Sizer
from typing import Dict, Any
from utils.debug_log_funcs import RunTimer


def get_google_api_key() -> str:

    timer = RunTimer('GET_API_KEY-- получение API ключа')

    config = ThirdPartServicesSettings.get_solo()

    timer.stop_and_log()

    return config.google_places_api_key


def get_search_constant() -> float:
    config = ThirdPartServicesSettings.get_solo()
    return config.kilometer_constant


def get_sms_credentials() -> Dict[str, Any]:
    config = ThirdPartServicesSettings.get_solo()
    return {
        'login': config.sms_service_login,
        'password': config.sms_service_password
    }


def get_pseudo_counter() -> str:
    the_model = PseudoCounter.get_solo()
    return the_model.count


def is_sms_service_enabled() -> bool:
    return ThirdPartServicesSettings.sms_service_enabled


def get_site_data() -> Dict[str, Any]:
    settings = SiteSettings.get_solo()
    return {
        "phone": settings.phone,
        "email": settings.email,
        "developer": settings.developer,
        "header_name": settings.header_name,
        "center_text": settings.center_text,
        "footer_name": settings.footer_name
    }


def get_profile_size():
    config = Sizer.get_solo()
    return {
        "width": config.width_profile,
        "height": config.height_profile
    }
