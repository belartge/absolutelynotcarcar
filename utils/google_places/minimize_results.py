from typing import Dict, Any, List


def get_full_name_by_components(components: List[object]) -> str:

    ret_val = ''

    for component in components[:]:
        ret_val += component['long_name'] + ', '

    return ret_val[:]


def minimize_google_predictions(diction: Dict[str, Any]) -> Dict[str, Any]:

    # todo переделать (не всегда самый последний клмпонент - индекс)

    ret_val = []

    for item in diction['predictions']:
        cur_item = dict()

        cur_item['full_name'] = item['description']
        cur_item['place_id'] = item['place_id']
        cur_item['short_name'] = item['terms'][0]['value']

        ret_val.append(cur_item)

    return ret_val


def minimize_google_detail(diction: Dict[str, Any]) -> Dict[str, Any]:

    ret_val = dict()

    location = diction['result']['geometry']['location']

    ret_val['location'] = [location['lat'], location['lng']]
    ret_val['full_name'] = get_full_name_by_components(diction['result']['address_components'])
    ret_val['short_name'] = diction['result']['name']

    return ret_val
