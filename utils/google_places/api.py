from typing import Dict, Any

import requests

from utils.app_settings.app_settings_getters import get_google_api_key

URL_FOR_AUTOCOMPLETE = 'https://maps.googleapis.com/maps/api/place/autocomplete/json'
URL_FOR_DETAIL = 'https://maps.googleapis.com/maps/api/place/details/json'


def fields_to_str(fields: Dict) -> str:
    return '&'.join('{}={}'.format(key, val) for (key, val) in fields.items())


# def get_google_api_key() -> str:
#     return 'AIzaSyBiJLN43xDCBrMYO11gS9X2qTdy-MsYVZU'


def autocomplete_by_location(session_token: str, input_string: str, city_location: str, language: str = 'ru') -> Dict[str, Any]:

    fields = {
        'key': get_google_api_key(),
        'session_token': session_token,
        'language': language,
        'input': input_string,
        'location': city_location,
        'radius': 100
    }

    fields_str = fields_to_str(fields)
    url_with_params = URL_FOR_AUTOCOMPLETE + '?' + fields_str

    result = requests.get(url_with_params)
    return result.json()


def detail(session_token: str, place_id: str, language: str = 'ru') -> Dict[str, Any]:

    fields = {
        'key': get_google_api_key(),
        'session_token': session_token,
        'language': language,
        'place_id': place_id,
        'fields': 'name,geometry,formatted_address,address_components'
    }

    fields_str = fields_to_str(fields)
    url_with_params = URL_FOR_DETAIL + '?' + fields_str

    result = requests.get(url_with_params)
    return result.json()


def autocomplete(session_token: str, input_string: str, types: str = '', language: str = 'ru') -> Dict[str, Any]:

    if types != '':
        types = '&types=(' + types + ')'

    fields = {
        'key': get_google_api_key(),
        'session_token': session_token,
        'language': language,
        'input': input_string
    }

    fields_str = fields_to_str(fields)
    url_with_params = URL_FOR_AUTOCOMPLETE + '?' + fields_str + types

    result = requests.get(url_with_params)
    return result.json()
