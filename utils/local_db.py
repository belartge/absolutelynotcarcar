from jsondb.db import Database


def get_db_name(first_part: str, second_part: str = None) -> str:
    name_string = './local_databases/{0}_{1}.dbjson' if second_part else './local_databases/{0}.dbjson'
    return name_string.format(first_part, second_part)


def get_db(name: str) -> Database:
    return Database(name)
