import datetime

from django.conf import settings
from django.db import connection
from api.serializers import FullRouteSerializer
from dateutil.parser import parse


search_limit = 50


def generate_response(routes_objects, search_date, constant, location_from, location_to, request):
    return_array = []
    routes = list(routes_objects)
    for route in routes:
        # сериализуем маршрут
        current_serialized_route = FullRouteSerializer(route, context={
                'request': request
            }).data

        # ближайшая даты машрута
        nearest_date = get_nearest_date(parse(current_serialized_route['date_time_start']),
                                        parse(current_serialized_route['date_time_end']),
                                        current_serialized_route['schedule'],
                                        search_date)

        # если ближайшая дата существует (если нет, то такой маршрут не подходит)
        if nearest_date:
            # заполняем дистанции у остановок
            for stop in current_serialized_route['stops']:
                stop.update({'distance_from': get_distance(location_from, stop)})
                stop.update({'distance_to': get_distance(location_to, stop)})

            # вставляем ближайшую дату маршрута
            current_serialized_route['nearest_date'] = nearest_date.replace(tzinfo=datetime.timezone.utc).isoformat()

            # ищем ближайшие к поиску остановки
            stops_from = get_stops_from(current_serialized_route, constant, location_from)
            stops_to = get_stops_to(current_serialized_route, constant, location_to)

            # если ближайшие остановки найдены
            if len(stops_from) != 0 and len(stops_to) != 0:
                # сколько минут прошло с начала маршрута, до to/from остановки
                from_minutes = get_count_minutes_from_start(current_serialized_route['stops'], stops_from[0]['id'])
                to_minutes = get_count_minutes_from_start(current_serialized_route['stops'], stops_to[0]['id'])

                # оставляем дату, чтобы потом отсортировать
                current_serialized_route['date_time_from'] = (nearest_date + datetime.timedelta(minutes=from_minutes)).replace(tzinfo=datetime.timezone.utc)
                current_serialized_route['date_time_to'] = (nearest_date + datetime.timedelta(minutes=to_minutes)).replace(tzinfo=datetime.timezone.utc).isoformat()

                # помечаем остановки как selected/not selected
                for stop in current_serialized_route['stops']:
                    if stop['id'] in [stops_from[0]['id'], stops_to[0]['id']]:
                        stop['is_selected'] = True
                    else:
                        stop['is_selected'] = False
            return_array.append(current_serialized_route)

    # сортируем
    return_array.sort(key=lambda x: x['date_time_from'])

    # сериализуем дату
    for route in return_array:
        route['date_time_from'] = route['date_time_from'].isoformat()

    return return_array


def get_count_minutes_from_start(stops, selected_stop_id):
    sum = 0
    for i in range(1, len(stops)):
        stop = stops[i]
        sum += stop['minutes']
        if stop['id'] == selected_stop_id:
            return sum
    return 0


def get_stops_from(route, constant, location_from):
    stops = list(filter(lambda stop: is_selected_from_stop(stop, constant, location_from), route['stops'].copy()))
    return sorted(stops, key=lambda item: item['distance_from'])


def get_stops_to(route, constant, location_to):
    stops = list(filter(lambda stop: is_selected_to_stop(stop, constant, location_to), route['stops'].copy()))
    return sorted(stops, key=lambda item: item['distance_to'])


def get_distance(location, stop):
    diff_lat = stop['place']['lat'] - location[0]
    diff_lng = stop['place']['lng'] - location[1]
    return diff_lat ** 2 + diff_lng ** 2


def is_selected_stop(stop, constant, location_from, location_to):
    if stop['place']['lat'] - constant <= location_from[0] <= stop['place']['lat'] + constant and \
       stop['place']['lng'] - constant <= location_from[1] <= stop['place']['lng'] + constant or \
       stop['place']['lat'] - constant <= location_to[0] <= stop['place']['lat'] + constant and \
       stop['place']['lng'] - constant <= location_to[1] <= stop['place']['lng'] + constant:
        return True
    return False


def is_selected_from_stop(stop, constant, location_from):
    if stop['place']['lat'] - constant <= location_from[0] <= stop['place']['lat'] + constant and \
       stop['place']['lng'] - constant <= location_from[1] <= stop['place']['lng'] + constant:
        return True
    return False


def is_selected_to_stop(stop, constant, location_to):
    if stop['place']['lat'] - constant <= location_to[0] <= stop['place']['lat'] + constant and \
       stop['place']['lng'] - constant <= location_to[1] <= stop['place']['lng'] + constant:
        return True
    return False


def get_search_sql(search_date):
    if settings.DATABASES['default']['ENGINE'] == 'django.db.backends.postgresql_psycopg2':
        date_str = search_date.strftime("%Y-%m-%d %H:%M:%S")
        date_str_sql = "date '" + date_str + "'"
        sql_str = """
                        SELECT DISTINCT T1.route_id
                        FROM api_stop AS T1, api_stop AS T2, api_place AS P1, api_place AS P2,  api_route AS R
                        WHERE T1.place_id = P1.google_place_id AND R.id = T1.route_id AND  T2.place_id = P2.google_place_id AND
                        P1.lat > %s AND P1.lat < %s AND P1.lng > %s AND P1.lng < %s AND 
                        P2.lat > %s AND P2.lat < %s AND P2.lng > %s AND P2.lng < %s AND
                        T1.route_id = T2.route_id AND T1.order < T2.order
                        AND """ + date_str_sql + """ < R.date_time_end
                        AND NOT R.is_deleted
                        LIMIT """ + str(search_limit) + """;
                    """
    else:
        search_timestamp = search_date.timestamp()
        date_str_sql = "datetime(" + str(search_timestamp) + ", 'unixepoch')"
        sql_str = """
                        SELECT DISTINCT T1.'route_id'
                        FROM 'api_stop' AS T1, 'api_stop' AS T2, 'api_place' AS P1, 'api_place' AS P2,  'api_route' AS R
                        WHERE T1.'place_id' = P1.'google_place_id' AND R.'id' = T1.'route_id' AND  T2.'place_id' = P2.'google_place_id' AND
                        P1.'lat' > %s AND P1.'lat' < %s AND P1.'lng' > %s AND P1.'lng' < %s AND 
                        P2.'lat' > %s AND P2.'lat' < %s AND P2.'lng' > %s AND P2.'lng' < %s AND
                        T1.'route_id' = T2.'route_id' AND T1.'order' < T2.'order'
                        AND """ + date_str_sql + """ < R.'date_time_end'
                        AND NOT R.is_deleted
                        LIMIT """ + str(search_limit) + """;
                    """
    return sql_str


def is_locations_near(from_location, to_location, constant):
    return abs(from_location[0] - to_location[0]) < constant \
           and abs(from_location[1] - to_location[1]) < constant


def get_route_ids_from_db(search_date, from_location, to_location, constant):
    sql_str = get_search_sql(search_date)
    with connection.cursor() as cursor:
        cursor.execute(sql_str, [from_location[0] - constant, from_location[0] + constant,
                                 from_location[1] - constant, from_location[1] + constant,
                                 to_location[0] - constant, to_location[0] + constant,
                                 to_location[1] - constant, to_location[1] + constant])
        route_ids = []
        for row in cursor.fetchall():
            route_ids.append(row[0])

        return route_ids
    return None


def _get_closest_to_start(route_date_time_start: datetime,
                          route_date_time_end: datetime,
                          route_schedule: str):
    for delta in range(0, (route_date_time_end - route_date_time_start).days + 1):
        day = route_date_time_start + datetime.timedelta(days=delta)
        if route_schedule[day.weekday()] == '+':
            return day
    return None


def get_nearest_date(route_date_time_start: datetime,
                     route_date_time_end: datetime,
                     route_schedule: str,
                     search_date_time: datetime):
    if route_date_time_start.timestamp() > search_date_time.timestamp():
        return _get_closest_to_start(route_date_time_start, route_date_time_end, route_schedule)
    if route_date_time_end.timestamp() < search_date_time.timestamp():
        return None

    minimal_start_date = search_date_time

    if minimal_start_date.hour > route_date_time_start.hour:
        minimal_start_date += datetime.timedelta(days=1)
    elif minimal_start_date.hour == route_date_time_start.hour and minimal_start_date.minute > route_date_time_start.minute:
        minimal_start_date += datetime.timedelta(days=1)

    minimal_start_date = datetime.datetime(year=minimal_start_date.year, month=minimal_start_date.month,
                                           day=minimal_start_date.day, hour=route_date_time_start.hour,
                                           minute=route_date_time_start.minute)

    for delta in range(0, (route_date_time_end - search_date_time).days):
        day = minimal_start_date + datetime.timedelta(days=delta)
        if route_schedule[day.weekday()] == '+':
            return day

    return None
