from django.http import JsonResponse
from rest_framework import status


def error(status, header, message):
    return JsonResponse(
        status=status,
        data={
            "header": header,
            "message": message
        }
    )


def unauthed():
    return {'status': status.HTTP_401_UNAUTHORIZED,
            'header': "unauthed",
            'message': "Вы не авторизованы"}


def notfound(id=None):
    if id is None:
        id = ""
    else:
        id = id + " "
    return {'status': status.HTTP_404_NOT_FOUND,
            'header': "notfound",
            'message': "Пользователь " + id + "не найден"}


def collision(email=None, phone=None):
    if phone is None:
        cause = "адресом электронной почты"
    else:
        cause = "номером телефона"
    return {'status': status.HTTP_406_NOT_ACCEPTABLE,
            'header': "collision",
            'message': "Пользователь с таким " + cause + " уже существует"}


def notequal():
    return {'status': status.HTTP_409_CONFLICT,
                 'header': "notequal",
                 'message': "Код не совпал с высланным по СМС кодом"}


def notasked():
    return {'status': status.HTTP_404_NOT_FOUND,
                 'header': "notasked",
                 'message': "Запрос на регистрацию не поступал"}


def notconfirmed():
    return {'status': status.HTTP_403_FORBIDDEN,
                 'header': "notconfirmed",
                 'message': "Запрос на восстановление не подтвержден"}


def notrightlogin():
    return {'status': status.HTTP_400_BAD_REQUEST,
                 'header': "notlogin",
                 'message':"Неверный логин и/или пароль"}


def notadequate(the_field='телефон'):
    return {'status': status.HTTP_400_BAD_REQUEST,
            'header': 'notadequate',
            'message': 'Невалидные данные в поле ' + the_field}


def notfar():
    return {'status': status.HTTP_411_LENGTH_REQUIRED,
            'header': 'notfar',
            'message': 'Точки расположены близко'}


def nocheats():
    return {'status': status.HTTP_412_PRECONDITION_FAILED,
            'header': 'nocheats',
            'message': 'Вы не можете создавать маршруты за других'}


def noschedule():
    return {'status': status.HTTP_417_EXPECTATION_FAILED,
            'header': 'noschedule',
            'message': 'Неверный формат расписания'}


def nohomo():
    return {'status': status.HTTP_422_UNPROCESSABLE_ENTITY,
            'header': 'nohomo',
            'message': '#nohomo'}


def nocar():
    return {'status': 420,
            'header': 'shitten',
            'message': 'У вас не задана машина'}


def busy():
    return {'status': 421,
            'header': 'busy',
            'message': 'У пользователя в это время задана поездка'}


def phone_invalid():
    return {'status': 425,
            'header': 'phone_invalid',
            'message': 'Неверный формат номера телефона'}


def invalid_schedule():
    return {'status': 426,
            'header': 'invalid_schedule',
            'message': 'Дата конца не может быть раньше даты начала'}


def invalid_time_intervals():
    return {'status': 427,
            'header': 'invalid_time_intervals',
            'message': 'Нарушен порядок следования остановок'}

def server_error():
    return {'status': status.HTTP_500_INTERNAL_SERVER_ERROR,
            'header': 'server_error',
            'message': 'Ошибка сервера'}

