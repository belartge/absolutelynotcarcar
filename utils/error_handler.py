from psycopg2.extensions import JSON
from rest_framework.utils import json
from rest_framework.views import exception_handler

from utils.errors import *


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    print('hey')
    if response is not None:
        print(response.data)
        if 'non_field_errors' in response.data:
            if response.data['non_field_errors'][0].code == 'invalid':
                err = notrightlogin()
                for x, y in err.items():
                    response.data[x] = y
                response.status_code = response.data['status']

        elif 'detail' in response.data:
            if response.data['detail'] == 'Учетные данные не были предоставлены.':
                err = unauthed()
                for x, y in err.items():
                    response.data[x] = y
                response.status_code = response.data['status']
                # response.data['header'] = 'nini'
                # response.data['message'] = 'Учетные данные не были предоставлены.'
                del(response.data['detail'])
            elif response.data['detail'] == 'Given token not valid for any token type':
                err = unauthed()
                response.data['message'] = err.get('message')

        else:
            response.data['status_code'] = response.status_code
            # err = error(status=response.status_code, header="nini", message="Операция не может быть выполнена")
            # response.data['header'] = 'nini'
            response.data['message'] = 'Операция не может быть выполнена'

    return response
