from django.db import models
from django.utils import timezone
from django.utils.html import format_html

# photo
from easy_thumbnails.fields import ThumbnailerImageField
from easy_thumbnails.files import get_thumbnailer


class Slide(models.Model):

    header = models.CharField(
        max_length=50,
        verbose_name='Название'
    )

    link = models.CharField(
        max_length=200,
        verbose_name='Ссылка',
        blank=True,
        null=True
    )

    image = ThumbnailerImageField(
        upload_to='images/events',
        verbose_name='Ссылка фото'
    )

    def image_img(self):
        if self.image:
            return format_html('<a href="{}" target="_blank"><img src="{}" width="270px"/></a>',
                               get_thumbnailer(self.image).url,
                               get_thumbnailer(self.image).get_thumbnail({'size': (384, 112), 'crop': True}).url)
        else:
            return '(Нет изображения)'

    image_img.short_description = 'Фото'
    image_img.allow_tags = True

    order = models.PositiveIntegerField(
        default=0,
        blank=False,
        null=False
    )

    platforms = models.ManyToManyField(
        'Device',
        verbose_name='Платформы',
        blank=True
    )

    def platforms_str(self):
        retStr = ''
        platforms = list(self.platforms.values('name'))
        if len(platforms) > 0:

            # platf = list(self.platforms)
            return ', '.join(map(lambda x: x['name'], platforms))
            # return '1'
        else:
            return ''

    def __str__(self):
        return self.header

    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайды'
        ordering = ['order']


class Device(models.Model):

    name = models.CharField(
        max_length=50,
        verbose_name='Название'
    )

    name_for_query = models.CharField(
        max_length=50,
        verbose_name='Название для API запросов',
        help_text='необходимо писать латинскими прописными буквами'
    )

    size = models.CharField(
        max_length=50,
        verbose_name='Размер слайда',
        default='1020x560',
        help_text='<ширина>x<высота>'
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Платформа'
        verbose_name_plural = 'Платформы'
