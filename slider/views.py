from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.response import Response
from rest_framework import status, generics
from django.http import HttpResponse, JsonResponse

from .models import Slide, Device
from .serializers import SlideSerializer, DeviceSerializer


class SlideList(generics.ListAPIView):
    queryset = Slide.objects.all()
    serializer_class = SlideSerializer

    pagination_class = None

    def list(self, request, *args, **kwargs):

        device_str = self.request.query_params.get('platform', None)

        if device_str is not None:
            device = Device.objects.filter(name_for_query=device_str)

            if device.count() > 0:
                queryset = Slide.objects.filter(platforms=device.first()).distinct()
                serializer = SlideSerializer(queryset, many=True, context={
                    'size': device.first().size,
                    'request': request
                })
                return Response(serializer.data)
            else:
                return Response({'error': 'Неправильная платформа'}, status=status.HTTP_400_BAD_REQUEST)

        queryset = Slide.objects.all()
        serializer = SlideSerializer(queryset, many=True, context={
            'request': request
        })
        return Response(serializer.data)

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]


class PlatformList(generics.ListAPIView):
    queryset = Device.objects.all().order_by('pk')
    serializer_class = DeviceSerializer

    pagination_class = None

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]
