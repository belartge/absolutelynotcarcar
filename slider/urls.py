from rest_framework import routers
from django.conf.urls import url

from .views import SlideList, PlatformList

app_name = 'slider'
urlpatterns = [
    url(r'^slides/$', SlideList.as_view()),
    url(r'^platforms/$', PlatformList.as_view()),
]
