from django.contrib import admin
from django.forms import CheckboxSelectMultiple
from django.db import models

from adminsortable2.admin import SortableAdminMixin
from .models import Slide, Device


class SlideAdmin(SortableAdminMixin, admin.ModelAdmin):
    readonly_fields = ['image_img', ]

    list_display = ('header', 'image_img', 'platforms_str')
    list_filter = ['platforms']

    fields = ['header', 'link', ('image', 'image_img'), 'platforms']

    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }


admin.site.register(Slide, SlideAdmin)


admin.site.register(Device)
