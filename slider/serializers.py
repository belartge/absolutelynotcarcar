from rest_framework import serializers
from easy_thumbnails.files import get_thumbnailer

from .models import Slide, Device

from easy_thumbnails.files import get_thumbnailer


class SlideSerializer(serializers.ModelSerializer):

    cropped_image = serializers.SerializerMethodField('_cropped_image')
    platforms = serializers.StringRelatedField(many=True)

    def _cropped_image(self, obj):
        size = self.context.get('size')
        request = self.context.get('request')

        if size is not None:
            size_parts = size.split('x')
            photo_url = get_thumbnailer(obj.image).get_thumbnail({'size': (size_parts[0], size_parts[1]), 'crop': True}).url
            return request.build_absolute_uri(photo_url)
        else:
            return None

    class Meta:
        model = Slide
        fields = ('id', 'header', 'link', 'image', 'cropped_image', 'order', 'platforms',)
        # depth = 1


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = ('id', 'name', 'name_for_query', 'size')
