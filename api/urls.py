from django.urls import path
from django.conf.urls import url
from . import views
from rest_framework import routers
from django.views.generic import TemplateView
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from .views import UserViewSet, StopViewSet, RouteViewSet, search

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'stop', StopViewSet)
router.register(r'route', RouteViewSet)

app_name = 'api'
urlpatterns = [
    # path('', TemplateView.as_view(template_name="main/pages/index.html"), name="index"),
    url(r'^cities/$', views.city_list),
    url(r'^login/$', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    url(r'^refresh_token/$', TokenRefreshView.as_view(), name='token_refresh'),
    url(r'^confirm/$', views.confirmation),
    url(r'^search/$', views.search),
    url(r'^users/send_code/$', views.confirm_send_code),
    url(r'^start_reg/$', views.start_registration),
    url(r'^finish_reg/$', views.finish_registration),
    url(r'^users/start_subscr/$', views.start_buy_subscription),
    url(r'^users/confirm_subscr/$', views.finish_buy_subscription),
    url(r'^users/repair/phone$', views.start_phone_reparation),
    url(r'^users/repair/email$', views.start_email_reparation),
    url(r'^users/repair/confirm/$', views.confirm_reparation),
    url(r'^users/repair/set_new_password/$', views.set_new_pass_reparation),
    url(r'^counter/$', views.get_counter),
    url(r'^index/$', views.get_site_info),
    url(r'^veryimportantcostyl/$', views.get_sms_balance),
    url(r'^dodangerousstuff/$', views.do_dangerous),
]

urlpatterns += router.urls
