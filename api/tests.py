import datetime

from django.test import TestCase

from utils.search.search_utils import get_nearest_date


class SearchTestClass(TestCase):
    def print_conditionals(self, header, start, end, schedule, search, expected):
        print()
        print(header)
        print("Маршрут: {0} - {1}".format(start, end))
        print("расписание: {0}".format(schedule))
        print("дата/время поиска: {0}".format(search))
        print("Ожидаемый возврат от функции: {0}".format(expected))
        print('---------')

    def test_usual_route_to_end(self):
        test_header = "Проверка обычного маршрута, до конца маршрута"
        route_date_time_start = datetime.datetime(2018, 12, 25, 11, 30)
        route_date_time_end = datetime.datetime(2018, 12, 25, 11, 30)
        route_schedule = '+++++++'
        search_date_time = datetime.datetime(2018, 12, 25, 11, 00)
        expected_result = datetime.datetime(2018, 12, 25, 11, 30)

        self.print_conditionals(test_header, route_date_time_start, route_date_time_end,
                                route_schedule, search_date_time, expected_result)

        self.assertEqual(get_nearest_date(route_date_time_start=route_date_time_start,
                                          route_date_time_end=route_date_time_end,
                                          route_schedule=route_schedule,
                                          search_date_time=search_date_time), expected_result)

    def test_usual_route_after_end(self):
        test_header = "Проверка обычного маршрута, после того как маршрут закончился"
        route_date_time_start = datetime.datetime(2018, 12, 25, 11, 30)
        route_date_time_end = datetime.datetime(2018, 12, 25, 11, 30)
        route_schedule = '+++++++'
        search_date_time = datetime.datetime(2018, 12, 26, 11, 00)
        expected_result = None

        self.print_conditionals(test_header, route_date_time_start, route_date_time_end,
                                route_schedule, search_date_time, expected_result)

        self.assertEqual(get_nearest_date(route_date_time_start=route_date_time_start,
                                          route_date_time_end=route_date_time_end,
                                          route_schedule=route_schedule,
                                          search_date_time=search_date_time), expected_result)

    def test_repeating_route_while_the_day(self):
        test_header = "Проверка повторяющегося маршрута, когда сегодняшний маршрут еще не прошел"
        route_date_time_start = datetime.datetime(2018, 12, 1, 11, 30)
        route_date_time_end = datetime.datetime(2018, 12, 28, 11, 30)
        route_schedule = '+++++--'
        search_date_time = datetime.datetime(2018, 12, 25, 11, 00)
        expected_result = datetime.datetime(2018, 12, 25, 11, 30)

        self.print_conditionals(test_header, route_date_time_start, route_date_time_end,
                                route_schedule, search_date_time, expected_result)

        self.assertEqual(get_nearest_date(route_date_time_start=route_date_time_start,
                                          route_date_time_end=route_date_time_end,
                                          route_schedule=route_schedule,
                                          search_date_time=search_date_time), expected_result)

    def test_repeating_route_after_the_day(self):
        test_header = "Проверка повторяющегося маршрута, когда сегодняшний маршрут уже прошел"
        route_date_time_start = datetime.datetime(2018, 12, 1, 11, 30)
        route_date_time_end = datetime.datetime(2018, 12, 28, 11, 30)
        route_schedule = '+++++--'
        search_date_time = datetime.datetime(2018, 12, 25, 11, 35)
        expected_result = datetime.datetime(2018, 12, 26, 11, 30)

        self.print_conditionals(test_header, route_date_time_start, route_date_time_end,
                                route_schedule, search_date_time, expected_result)

        self.assertEqual(get_nearest_date(route_date_time_start=route_date_time_start,
                                          route_date_time_end=route_date_time_end,
                                          route_schedule=route_schedule,
                                          search_date_time=search_date_time), expected_result)

    def test_finished_repeating_route_after_the_day(self):
        test_header = "Проверка повторяющегося маршрута, когда расписание совместно с датой/временем конца закончилось"
        route_date_time_start = datetime.datetime(2018, 12, 1, 11, 30)
        route_date_time_end = datetime.datetime(2018, 12, 30, 11, 30)
        route_schedule = '+++++--'
        search_date_time = datetime.datetime(2018, 12, 28, 11, 35)
        expected_result = None

        self.print_conditionals(test_header, route_date_time_start, route_date_time_end,
                                route_schedule, search_date_time, expected_result)

        self.assertEqual(get_nearest_date(route_date_time_start=route_date_time_start,
                                          route_date_time_end=route_date_time_end,
                                          route_schedule=route_schedule,
                                          search_date_time=search_date_time), expected_result)

    def test_after_route_finished(self):
        test_header = "Проверка повторяющегося маршрута, после конца маршрута"
        route_date_time_start = datetime.datetime(2018, 12, 1, 11, 30)
        route_date_time_end = datetime.datetime(2018, 12, 30, 11, 30)
        route_schedule = '+++++--'
        search_date_time = datetime.datetime(2018, 12, 31, 11, 35)
        expected_result = None

        self.print_conditionals(test_header, route_date_time_start, route_date_time_end,
                                route_schedule, search_date_time, expected_result)

        self.assertEqual(get_nearest_date(route_date_time_start=route_date_time_start,
                                          route_date_time_end=route_date_time_end,
                                          route_schedule=route_schedule,
                                          search_date_time=search_date_time), expected_result)

    def test_minused(self):
        test_header = "Проверка повторяющегося маршрута, после конца маршрута"
        route_date_time_start = datetime.datetime(2018, 12, 1, 11, 30)
        route_date_time_end = datetime.datetime(2018, 12, 30, 11, 30)
        route_schedule = '-------'
        search_date_time = datetime.datetime(2018, 12, 3, 11, 35)
        expected_result = None

        self.print_conditionals(test_header, route_date_time_start, route_date_time_end,
                                route_schedule, search_date_time, expected_result)

        self.assertEqual(get_nearest_date(route_date_time_start=route_date_time_start,
                                          route_date_time_end=route_date_time_end,
                                          route_schedule=route_schedule,
                                          search_date_time=search_date_time), expected_result)
