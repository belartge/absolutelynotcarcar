from easy_thumbnails.files import get_thumbnailer
from rest_framework import serializers

from utils.app_settings.app_settings_getters import get_profile_size
from .models import City, Place, User, Route, Car, Stop


class CitySerializer(serializers.Serializer):
    short_name = serializers.CharField(required=True, max_length=50)
    full_name = serializers.CharField(required=True, max_length=200)
    lat = serializers.FloatField()
    lng = serializers.FloatField()

    def create(self, validated_data):
        return City.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.short_name = validated_data.get('short_name', instance.short_name)
        instance.full_name = validated_data.get('full_name', instance.full_name)
        instance.lat = validated_data.get('lat', instance.lat)
        instance.lng = validated_data.get('lng', instance.lng)
        instance.save()
        return instance

    class Meta:
        model = City
        fields = ('short_name', 'full_name', 'lat', 'lng')


class PlaceSerializer(serializers.Serializer):
    short_name = serializers.CharField(required=True, max_length=50)
    full_name = serializers.CharField(required=True, max_length=200)
    google_place_id = serializers.CharField(required=True, max_length=150)
    #  place_id = serializers.CharField(required=True, max_length=200)
    lat = serializers.FloatField()
    lng = serializers.FloatField()

    def create(self, validated_data):
        return Place.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.short_name = validated_data.get('short_name', instance.short_name)
        instance.full_name = validated_data.get('full_name', instance.full_name)
        # instance.place_id = validated_data.get('place_id', instance.place_id)
        instance.lat = validated_data.get('lat', instance.lat)
        instance.lng = validated_data.get('lng', instance.lng)
        instance.google_place_id = validated_data.get('google_place_id', instance.google_place_id)
        instance.save()
        return instance

    class Meta:
        model = Place
        fields = ('short_name', 'full_name', 'lat', 'lng', 'google_place_id')


class CarSerializer(serializers.Serializer):
    car_brend = serializers.CharField(required=True, max_length=30)
    car_number = serializers.CharField(required=True, max_length=20)

    def create(self, validated_data):
        return Car.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.car_brend = validated_data.get('car_brend', instance.car_brend)
        instance.car_number = validated_data.get('car_number', instance.car_number)
        instance.save()
        return instance

    class Meta:
        model = Car
        fields = ['car_brend', 'car_number']
        extra_kwargs = {
            'cars': {'lookup_field': 'car_number'}
        }


class ChoicesField(serializers.Field):
    def __init__(self, choices, **kwargs):
        self._choices = choices
        super(ChoicesField, self).__init__(**kwargs)

    def to_representation(self, obj):
        return self._choices[obj]

    def to_internal_value(self, data):
        return getattr(self._choices, data)


class UserSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    surname = serializers.CharField(required=True, max_length=40)
    name = serializers.CharField(required=True, max_length=30)
    patronym = serializers.CharField(required=False, max_length=40)
    email = serializers.CharField(required=True, max_length=100)
    phone = serializers.CharField(required=True, max_length=20)
    birth_date = serializers.DateField(required=True)
    subscribed_until = serializers.DateTimeField(required=True)
    confirmed_user = serializers.BooleanField()
    cropped_image = serializers.SerializerMethodField('_cropped_image')
    about = serializers.CharField(required=False, max_length=500)
    gender = serializers.CharField(required=False, max_length=7)
    city = CitySerializer()
    car = CarSerializer()

    def create(self, validated_data):
        user = User.objects.create(**validated_data)
        return user

    def _cropped_image(self, obj):
        if not obj.photo:
            return None
        width = get_profile_size()['width']
        height = get_profile_size()['height']
        request = self.context.get('request')
        photo_url = get_thumbnailer(obj.photo).get_thumbnail({'size': (width, height), 'crop': True}).url
        return request.build_absolute_uri(photo_url)

    def update(self, instance, validated_data):
        surname = validated_data.get('surname', instance.surname)
        name = validated_data.get('name', instance.name)
        patronym = validated_data.get('patronym', instance.patronym)
        gender = validated_data.get('gender', instance.gender)
        email = validated_data.get('email', instance.email)
        phone = validated_data.get('phone', instance.phone)
        birth_date = validated_data.get('birth_date', instance.birth_date)
        subscribed_until = validated_data.get('subscribed_until', instance.subscribed_until)
        confirmed_user = validated_data.get('confirmed_user', instance.confirmed_user)
        photo = validated_data.get('photo', instance.photo)
        about = validated_data.get('about', instance.about)

    class Meta:
        model = User
        fields = ['id', 'surname', 'name', 'photo', 'image', 'cropped_image', 'phone', 'email', 'city', 'car']
        extra_kwargs = {
            'users': {'lookup_field': 'phone'}
        }


class StopSerializer(serializers.ModelSerializer):
    place = PlaceSerializer()
    # route = RouteSerializer()
    minutes = serializers.IntegerField()
    order = serializers.IntegerField()

    def create(self, validated_data):
        return Stop.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.place = validated_data.get('place', instance.place)
        instance.route = validated_data.get('route', instance.route)
        instance.minutes = validated_data.get('minutes', instance.minutes)
        instance.order = validated_data.get('order', instance.order)
        instance.save()
        return instance

    class Meta:
        model = Stop
        fields = ('place', 'minutes', 'order', 'id')
        related_object = 'place'


class RouteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Route
        fields = ('date_time_start', 'date_time_end', 'driver', 'is_backward', 'schedule')


class FullRouteSerializer(serializers.ModelSerializer):
    driver = UserSerializer()
    stops = StopSerializer(many=True)

    class Meta:
        model = Route
        fields = ('id', 'date_time_start', 'date_time_end', 'driver', 'is_backward', 'schedule', 'stops', 'driver')
