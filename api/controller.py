import datetime
import random
import time
import uuid

from dateutil.relativedelta import *
from django.http import HttpResponse
from django.utils import timezone
from rest_framework.generics import get_object_or_404

from utils.app_settings.app_settings_getters import is_sms_service_enabled
from utils.errors import *
from utils.smsc_api import SMSC
from .models import User, PhoneCode, SubscriptionCode, RepairPhoneCode, RepairEmailCode, City, PhoneChangingCode, Car


def new_start_registration(request):
    phone = request['phone']
    if len(phone) < 3:
        err = notadequate()
        return error(status=err['status'], message=err['message'], header=err['header'])
    if not user_exist(phone=phone):
        send_code(phone=phone)
        return HttpResponse(status=status.HTTP_200_OK)
    err = collision(phone=phone)
    return error(status=err['status'], message=err['message'], header=err['header'])


def new_registrate(request):
    phone = request['phone']
    if len(phone) < 3:
        err = notadequate()
        return error(status=err['status'], message=err['message'], header=err['header'])
    if user_exist(phone):
        err = collision(phone=request.data['phone'])
        return error(status=err['status'], message=err['message'], header=err['header'])
    if phone in PhoneCode.objects.order_by().values_list('phone', flat=True):
        confirmer = PhoneCode.objects.get(phone=phone)
        if confirmer.code != request['code']:
            err = notequal()
            return error(status=err['status'], message=err['message'], header=err['header'])
        if request['gender'] not in ['мужской', 'женский']:
            err = nohomo()
            return error(status=err['status'], message=err['message'], header=err['header'])
        if request['phone'] not in User.objects.order_by().values_list('phone', flat=True):
            if request['email'] in User.objects.order_by().values_list('email', flat=True):
                err = collision(email=request['email'])
                return error(status=err['status'], message=err['message'], header=err['header'])
            patronym = ""
            if 'patronym' in request.keys():
                patronym = request['patronym']
            about = ""
            if 'about' in request.keys():
                about = request['about']
            if request['city']['full_name'] in City.objects.values_list('full_name', flat=True):
                city = City.objects.get(full_name=request['city']['full_name'])
            else:
                city = City.objects.create(
                    short_name=request['city']['short_name'],
                    full_name=request['city']['full_name'],
                    lat=request['city']['lat'],
                    lng=request['city']['lng']
                )
            user = User.objects.create_user(
                name=request['name'],
                surname=request['surname'],
                patronym=patronym,
                email=request['email'],
                phone=request['phone'],
                password=request['password'],
                birth_date=request['birth_date'],
                city=city,
                about=about,
                gender=request['gender'],
            )
            user.confirmed_user = True
            user.save()
            confirmer.delete()
            return JsonResponse({
                'name': request['name'],
                'surname': request['surname'],
                'patronym': patronym,
                'email': request['email'],
                'phone': request['phone'],
                'password': request['password'],
                'birth_date': request['birth_date'],
                'city': request['city'],
                'about': about,
            })
        err = notasked()
        return error(status=err['status'], message=err['message'], header=err['header'])


def send_code(phone):
    if is_sms_service_enabled():
        phone_code, created = PhoneCode.objects.get_or_create(phone=phone)
        phone_code.code = "00000"
        phone_code.save()
        return HttpResponse(status=status.HTTP_200_OK)
    else:
        new_code = random.randint(10000, 99999)
        while new_code in PhoneCode.objects.order_by().values_list('code', flat=True):
            new_code = random.randint(10000, 99999)
        phone_code, created = PhoneCode.objects.get_or_create(phone=phone)
        phone_code.code = new_code
        phone_code.save()
        sms_service = SMSC()
        sms_service.norm_sms_send(
            phone=phone,
            sender='CarCarCity',
            message='Your confirmation code: ' + str(new_code)
        )
        return HttpResponse(status=status.HTTP_200_OK)


def send_code_for_changing(phone, new_phone):
    if is_sms_service_enabled():
        phone_code, created = PhoneChangingCode.objects.get_or_create(phone=phone)
        phone_code.new_phone = new_phone
        phone_code.code = "00000"
        phone_code.save()
        return 200
    else:
        new_code = random.randint(10000, 99999)
        while new_code in PhoneChangingCode.objects.order_by().values_list('code', flat=True):
            new_code = random.randint(10000, 99999)
        phone_code, created = PhoneChangingCode.objects.get_or_create(phone=phone)
        phone_code.new_phone = new_phone
        phone_code.code = new_code
        phone_code.save()
        sms_service = SMSC()
        sms_service.norm_sms_send(
            phone=new_phone,
            sender='CarCarCity',
            message='Your confirmation code: ' + str(new_code)
        )
        return 200


def confirmation(phone):
    if user_exist(phone=phone) or phone in PhoneCode.objects.order_by().values_list('phone', flat=True):
        return HttpResponse(status=403)
    new_code = random.randint(10000, 99999)
    while new_code in RepairPhoneCode.objects.order_by().values_list('code', flat=True):
        new_code = random.randint(10000, 99999)
    phone_code, created = PhoneCode.objects.get_or_create(phone=phone, code=new_code)

    # return 200
    return HttpResponse(status=200)


def start_subscription(phone, months):
    if user_exist(phone=phone):
        # TODO set cost_admin from admin, not hardcode like this
        cost_admin = 50
        print(phone, ' wants to subscribe for ', months, 'months')
        # TODO sending request to kassa
        bank_request = {
            'phone': phone,
            'cost': cost_admin * int(months)
        }
        SubscriptionCode.objects.create(phone=phone, months=months)
        print('send to kassa request ', bank_request)
        return HttpResponse(status=status.HTTP_200_OK)
    err = notfound(phone)
    return error(status=err['status'], message=err['message'], header=err['header'])


def finish_subscription(phone, money):
    if user_exist(phone=phone) and phone in SubscriptionCode.objects.order_by().values_list('phone', flat=True):
        # TODO set cost_admin from admin, not hardcode like this
        cost_admin = 50
        money = int(money)
        months = money / cost_admin
        print(phone, ' confimed to subscribe for ', months, 'months')
        # TODO sending request to kassa
        user = get_object_or_404(User, phone=phone)
        millis = int(round(time.time() * 1000))

        if user.subscribed_until < timezone.now():
            now = datetime.datetime.now()
            user.subscribed_until = now + relativedelta(months=+int(months))
        else:
            user.subscribed_until = user.subscribed_until + relativedelta(months=+int(months))
        user.save()
        return HttpResponse(status=status.HTTP_200_OK)
    err = notfound(phone)
    return error(status=err['status'], message=err['message'], header=err['header'])


def start_repair_phone(phone):
    if user_exist(phone=phone):
        new_code = random.randint(10000, 99999)
        while new_code in RepairPhoneCode.objects.order_by().values_list('code', flat=True):
            new_code = random.randint(10000, 99999)
        phone_code, created = RepairPhoneCode.objects.get_or_create(phone=phone)
        print(phone_code, created)
        phone_code.code = new_code
        phone_code.operationId = uuid.uuid4()
        phone_code.confirmed = False
        phone_code.save()
        print('let we send a code ', new_code, ' at phone ', phone)
        return {
            'code': 200,
            'id': phone_code.operationId
        }
    return {
        'code': 404,
    }


def start_repair_email(email):
    if email in User.objects.order_by().values_list('email', flat=True):
        new_code = random.randint(10000, 99999)
        while new_code in RepairEmailCode.objects.order_by().values_list('code', flat=True):
            new_code = random.randint(10000, 99999)
        email_code, created = RepairEmailCode.objects.get_or_create(email=email)
        print(email_code, created)
        email_code.code = new_code
        email_code.operationId = uuid.uuid4()
        email_code.confirmed = False
        email_code.save()
        print('let we send a code ', new_code, ' at phone ')
        return {
            'code': 200,
            'id': email_code.operationId
        }
    return {
        'code': 404,
    }


def repair_confirm(operation_id, code):
    print('start confirmation ', operation_id, 'by code', code)
    if operation_id in RepairEmailCode.objects.order_by().values_list('operationId', flat=True):
        print('by email')
        emailer = get_object_or_404(RepairEmailCode, operationId=operation_id)
        if emailer.code == code:
            print('confirmed')
            emailer.confirmed = True
            emailer.save()
        return 200
    if operation_id in RepairPhoneCode.objects.order_by().values_list('operationId', flat=True):
        print('by phone')
        emailer = get_object_or_404(RepairPhoneCode, operationId=operation_id)
        if emailer.code == code:
            print('confirmed')
            emailer.confirmed = True
            emailer.save()
        return 200
    print('not confirmed')
    return 403


def set_new_password(operation_id, new_password):
    print('start setting new code')
    if operation_id in RepairEmailCode.objects.order_by().values_list('operationId', flat=True):
        emailer = get_object_or_404(RepairEmailCode, operationId=operation_id)
        if emailer.confirmed:
            user = User.objects.get(email=emailer.email)
            user.set_password(new_password)
            print('new password: ', new_password)
            user.save()
            emailer.delete()
            return 200
        else:
            print('not confirmed')
            return 403
    if operation_id in RepairPhoneCode.objects.order_by().values_list('operationId', flat=True):
        emailer = get_object_or_404(RepairPhoneCode, operationId=operation_id)
        if emailer.confirmed:
            user = User.objects.get(phone=emailer.phone)
            user.set_password(new_password)
            print('new password: ', new_password)
            user.save()
            emailer.delete()
            return 200
        else:
            return 403
    return 403


def user_exist(phone):
    return phone in User.objects.order_by().values_list('phone', flat=True)
