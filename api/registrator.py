import random

from rest_framework.generics import get_object_or_404

from .models import User, PhoneCode


def new_start_registration(request):
    print()


def registrate(request):
    # if 'patronym' in request.data.keys():
    #     patronym = request.data['patronym']
    if request['phone'] not in PhoneCode.objects.order_by().values_list('phone', flat=True):
        if request['phone'] not in User.objects.order_by().values_list('phone', flat=True) and request[
            'email'] not in User.objects.order_by().values_list('email', flat=True):
            patronym = ""
            if 'patronym' in request.keys():
                patronym = request['patronym']
            User.objects.create_user(
                name=request['name'],
                surname=request['surname'],
                patronym=patronym,
                email=request['email'],
                phone=request['phone'],
                password=request['password'],
                birth_date=request['birth_date'],
            )
            new_code = random.randint(10000, 99999)
            while new_code in PhoneCode.objects.order_by().values_list('code', flat=True):
                new_code = random.randint(10000, 99999)
            PhoneCode.objects.create(code=new_code, phone=request['phone'])
            send_code(request['phone'], new_code)
            return 200
    return 301


def send_code(phone, code):
    print('let we send a code ', code, ' at phone ', phone)


def confirmation(phone, code):
    coder = get_object_or_404(PhoneCode, phone=phone)
    if coder.code == code:
        user = User.objects.get(phone=phone)
        user.confirmed_user = True
        user.save()
        coder.delete()
        return 200
    return 400
