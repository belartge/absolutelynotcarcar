import re
from datetime import timedelta

import dateutil
from dateutil import parser
from dateutil.parser import parse
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets, status
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer

from utils.app_settings.app_settings_getters import get_pseudo_counter, get_site_data, get_search_constant
from .controller import *
from .models import City, User, Route, Stop, Place, Car
from .permissions import IsAdminOrSelf
from .serializers import CitySerializer, UserSerializer, RouteSerializer, StopSerializer, CarSerializer
from utils.search.search_utils import generate_response, get_search_sql, is_locations_near, get_route_ids_from_db


@csrf_exempt
def city_list(request):
    if request.method == 'GET':
        cities = City.objects.all()
        serializer = CitySerializer(cities, many=True)
        return JsonResponse(serializer.data, safe=False)


def is_phone_valid(phone):
    masks = [r'^((\+7)+([0-9]){10})$']
    for mask in masks:
        if re.match(pattern=mask, string=phone):
            return True
    return False


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_field = 'id'
    permission_classes = (IsAdminOrSelf,)

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        # if self.action in ['list', 'retrieve', 'registration']:
        #     permission_classes = [AllowAny]
        # else:
        permission_classes = [IsAdminOrSelf]
        return [permission() for permission in permission_classes]

    @action(methods=['post', 'get'], detail=False)
    def profile(self, request):
        if request.user.is_authenticated:
            return JsonResponse(status=status.HTTP_200_OK, data=UserSerializer(instance=request.user, context={
                'request': request
            }).data, safe=False)
        else:
            return Http404

    @action(methods=['post', 'get'], detail=True)
    def routes(self, request, id=None):
        user = self.get_object()
        # serializer = RouteSerializer(data=request.data)
        # if serializer.is_valid():
        # user.set_route(serializer.data['route'])

        if request.method == 'GET':
            answer = get_routes(user=user, request=request)
            return JsonResponse(answer, safe=False)

        elif request.method == 'POST':
            if user.id != request.user.id:
                err = nocheats()
                return error(status=err['status'], header=err['header'], message=err['message'])
            if user.id not in Car.objects.values_list('user_id', flat=True):
                err = nocar()
                return error(status=err['status'], header=err['header'], message=err['message'])

            all_known_user_routes = Route.objects.filter(driver_id=user.id)
            requested_time = parse(request.data['date_time_start'])
            if requested_time in all_known_user_routes.values_list('date_time_start', flat=True):
                for x in all_known_user_routes.filter(date_time_start=requested_time):
                    if not x.is_deleted:
                        err = busy()
                        return error(status=err['status'], header=err['header'], message=err['message'])
            for stop_for_minutes in request.data['stops']:
                if stop_for_minutes['minutes'] <= 0:
                    err = invalid_time_intervals()
                    return error(status=err['status'], header=err['header'], message=err['message'])

            route = Route.objects.create(driver_id=user.id, date_time_start=request.data['date_time_start'])
            route.user = user
            route.date_time_start = request.data['date_time_start']
            if 'is_backward' in request.data.keys():
                route.is_backward = request.data['is_backward']
            if 'date_time_end' in request.data.keys():
                route.date_time_end = request.data['date_time_end']
            else:
                route.date_time_end = request.data['date_time_start']
            route.save()
            stops = request.data['stops']
            index = 0
            for stop in stops:
                if stop['place']['google_place_id'] in Place.objects.values_list('google_place_id', flat=True):
                    place = Place.objects.get(google_place_id=stop['place']['google_place_id'])
                else:
                    place = Place.objects.create(
                        short_name=stop['place']['short_name'],
                        full_name=stop['place']['full_name'],
                        lat=stop['place']['lat'],
                        lng=stop['place']['lng'],
                        google_place_id=stop['place']['google_place_id']
                    )
                stop_object, created = Stop.objects.get_or_create(
                    place=place,
                    route=route,
                    minutes=stop['minutes'],
                    order=index + 1
                )
                stop_object.save()
                index += 1
            return JsonResponse(status=status.HTTP_200_OK, data=get_route(user=user, route_id=route.id))

    @action(methods=['post'], detail=True)
    def edit(self, request, id=None):
        if request.method == 'POST':
            user = self.get_object()
            if user is None:
                ans = notfound()
                return error(status=ans['status'], header=ans['header'], message=ans['message'])
            for key, value in request.data.items():
                if key == 'email':
                    if request.data['email'] in User.objects.values_list('email', flat=True) and user.email != \
                            request.data['email']:
                        ans = collision(email=request.data['email'])
                        return error(status=ans['status'], header=ans['header'], message=ans['message'])
                if key == 'phone':
                    curr_phone = request.data['phone']
                    new_phone = value
                    if '(' in curr_phone:
                        curr_phone = curr_phone.replace(old="(", new="")
                    if ')' in curr_phone:
                        curr_phone = curr_phone.replace(old=")", new="")
                    if '-' in curr_phone:
                        curr_phone = curr_phone.replace(old="-", new="")
                    if ' ' in curr_phone:
                        curr_phone = curr_phone.replace(old=" ", new="")
                    if '(' in new_phone:
                        new_phone = new_phone.replace(old="(", new="")
                    if ')' in new_phone:
                        new_phone = new_phone.replace(old=")", new="")
                    if '-' in new_phone:
                        new_phone = new_phone.replace(old="-", new="")
                    if ' ' in new_phone:
                        new_phone = new_phone.replace(old=" ", new="")
                    if not is_phone_valid(curr_phone):
                        ans = phone_invalid()
                        return error(status=ans['status'], header=ans['header'], message=ans['message'])
                    if user.phone != new_phone:
                        if curr_phone in User.objects.values_list('phone', flat=True) and user.phone != curr_phone \
                                or new_phone in User.objects.values_list('phone', flat=True):
                            ans = collision(phone=request.data['phone'])
                            return error(status=ans['status'], header=ans['header'], message=ans['message'])

                        send_code_for_changing(user.phone, new_phone)
                elif key == 'city':
                    if request.data['city']['full_name'] in City.objects.values_list('full_name', flat=True):
                        city = City.objects.get(full_name=request.data['city']['full_name'])
                    else:
                        city = City.objects.create(
                            short_name=request.data['city']['short_name'],
                            full_name=request.data['city']['full_name'],
                            lat=request.data['city']['lat'],
                            lng=request.data['city']['lng']
                        )
                    user.city = city
                else:
                    setattr(user, key, value)
            user.save()
            return HttpResponse(status=200)

    @action(methods=['post', 'get'], detail=True)
    def car(self, request, id=None):
        user = self.get_object()
        if user is None:
            ans = notfound()
            return error(status=ans['status'], header=ans['header'], message=ans['message'])
        if request.method == 'GET':
            if user.id in Car.objects.values_list('user_id', flat=True):
                car = Car.objects.get(user_id=user.id)
                serializer = CarSerializer(instance=car, many=False)
                return JsonResponse(status=status.HTTP_200_OK, data=serializer.data)
            return JsonResponse(status=status.HTTP_200_OK, data={})
        elif request.method == 'POST':
            if len(request.data['car_number']) == 0:
                err = notadequate(the_field='Номер машины')
                return error(status=err['status'], header=err['header'], message=err['message'])
            if len(request.data['car_brend']) == 0:
                err = notadequate(the_field='Марка машины')
                return error(status=err['status'], header=err['header'], message=err['message'])
            if user.id in Car.objects.values_list('user_id', flat=True):
                car = Car.objects.get(user_id=user.id)
                car.delete()
            car = Car.objects.create(
                car_number=request.data['car_number'],
                car_brend=request.data['car_brend'],
                user_id=user.id,
            )
            car.save()
            serializer = CarSerializer(instance=car, many=False)
            return JsonResponse(status=status.HTTP_200_OK, data=serializer.data)

    @action(methods=['get'], detail=True)
    def history(self, request, id=None):
        user = self.get_object()

        answer = get_history_routes(user=user, request=request)
        return JsonResponse(answer, safe=False)

    @action(methods=['post'], detail=True)
    def confirm_code(self, request, id=None):
        if request.method == 'POST':
            user = self.get_object()
            if user is None:
                ans = notfound()
                return error(status=ans['status'], header=ans['header'], message=ans['message'])
            if user.phone not in PhoneChangingCode.objects.values_list('new_phone', flat=True):
                ans = notasked()
                return error(status=ans['status'], header=ans['header'], message=ans['message'])
            pcc = PhoneChangingCode.objects.get(phone=user.phone)
            if request.data['code'] == pcc.code:
                user.phone = pcc.new_phone
                user.save()
                pcc.delete()
                return HttpResponse(status=status.HTTP_200_OK)
            else:
                ans = notequal()
                return error(status=ans['status'], header=ans['header'], message=ans['message'])


def get_routes(user, request):
    routes = Route.objects.filter(driver=user).order_by('date_time_start')
    # if page is not None:
    #     serializer = self.get_serializer(page, many=True)
    #     return self.get_paginated_response(serializer.data)

    answer = []
    for x in routes:
        if not x.is_deleted:
            if x.date_time_end.timestamp() > timezone.now().timestamp():
                current_route = {}
                stop = Stop.objects.filter(route=x).order_by('order')
                serial = StopSerializer(instance=stop, many=True)
                serializer = RouteSerializer(x, many=False)
                current_route['driver'] = serializer.data['driver']
                current_route['route_id'] = x.id
                current_route['date_time_start'] = serializer.data['date_time_start']
                current_route['date_time_finish'] = x.date_time_end
                current_route['is_backward'] = serializer.data['is_backward']
                if len(x.schedule) != 0:
                    current_route['schedule'] = x.schedule
                else:
                    current_route['schedule'] = None
                current_route['stops'] = serial.data
                answer.append(current_route)
    return answer


def get_history_routes(user, request):
    routes = Route.objects.filter(driver=user).order_by('-date_time_start')
    # if page is not None:
    #     serializer = self.get_serializer(page, many=True)
    #     return self.get_paginated_response(serializer.data)

    answer = []
    for x in routes:
        if not x.is_deleted:
            if x.date_time_end.timestamp() <= timezone.now().timestamp():
                current_route = {}
                stop = Stop.objects.filter(route=x).order_by('order')
                serial = StopSerializer(instance=stop, many=True)
                serializer = RouteSerializer(x, many=False)
                current_route['driver'] = serializer.data['driver']
                current_route['route_id'] = x.id
                current_route['date_time_start'] = serializer.data['date_time_start']
                current_route['is_backward'] = serializer.data['is_backward']
                if len(x.schedule) != 0:
                    current_route['schedule'] = x.schedule
                current_route['stops'] = serial.data
                answer.append(current_route)
    return answer[:10]


def get_route(user, route_id):
    routes = Route.objects.get(id=route_id)
    # if page is not None:
    #     serializer = self.get_serializer(page, many=True)
    #     return self.get_paginated_response(serializer.data)
    current_route = {}
    stop = Stop.objects.filter(route=routes).order_by('order')
    serial = StopSerializer(instance=stop, many=True)
    serializer = RouteSerializer(routes, many=False)
    current_route['driver'] = serializer.data['driver']
    current_route['date_time_start'] = serializer.data['date_time_start']
    if len(routes.schedule) != 0:
        current_route['schedule'] = routes.schedule
    current_route['stops'] = serial.data
    return current_route


@api_view(['POST'])
@permission_classes((AllowAny,))
def confirm(request):
    if request.method == 'POST':
        return confirmation(request.data['phone'])


@api_view(['POST'])
@permission_classes((AllowAny,))
def start_registration(request):
    if request.method == 'POST':
        curr_phone = request.data['phone']
        if '(' in curr_phone:
            curr_phone = curr_phone.replace(old="(", new="")
        if ')' in curr_phone:
            curr_phone = curr_phone.replace(old=")", new="")
        if '-' in curr_phone:
            curr_phone = curr_phone.replace(old="-", new="")
        if ' ' in curr_phone:
            curr_phone = curr_phone.replace(old=" ", new="")
        if not is_phone_valid(curr_phone):
            ans = phone_invalid()
            return error(status=ans['status'], header=ans['header'], message=ans['message'])
        code = new_start_registration(request.data)
        return code


@api_view(['POST'])
@permission_classes((AllowAny,))
def finish_registration(request):
    if user_exist(request.data['phone']):
        err = collision(phone=request.data['phone'])
        return error(status=err['status'], message=err['message'], header=err['header'])
    if request.data['phone'] not in PhoneCode.objects.values_list('phone', flat=True):
        err = notasked()
        return error(status=err['status'], message=err['message'], header=err['header'])
    curr_phone = request.data['phone']
    if '(' in curr_phone:
        curr_phone = curr_phone.replace(old="(", new="")
    if ')' in curr_phone:
        curr_phone = curr_phone.replace(old=")", new="")
    if '-' in curr_phone:
        curr_phone = curr_phone.replace(old="-", new="")
    if ' ' in curr_phone:
        curr_phone = curr_phone.replace(old=" ", new="")
    if not is_phone_valid(curr_phone):
        ans = phone_invalid()
        return error(status=ans['status'], header=ans['header'], message=ans['message'])
    answer = new_registrate(request.data)
    return answer
    #
    # User.objects.create(request['body'])


@api_view(['POST'])
@permission_classes((AllowAny,))
def confirm_send_code(request):
    if request.method == 'POST':
        code = send_code(request.data['phone'])
        return JsonResponse({
            'code': code,
        })


@api_view(['POST'])
@permission_classes((AllowAny,))
def start_buy_subscription(request):
    if request.method == 'POST':
        months = 1
        if 'months' in request.data.keys():
            months = request.data['months']
        code = start_subscription(request.data['phone'], months)
        return JsonResponse({
            'code': code,
        })


@api_view(['POST'])
@permission_classes((AllowAny,))
def finish_buy_subscription(request):
    if request.method == 'POST':
        code = finish_subscription(request.data['phone'], request.data['money'])
        return JsonResponse({
            'code': code,
        })


@api_view(['GET'])
@permission_classes((AllowAny,))
def start_phone_reparation(request):
    if request.method == 'GET':
        code = start_repair_phone(request.query_params['phone'])
        return JsonResponse(code)


@api_view(['GET'])
@permission_classes((AllowAny,))
def start_email_reparation(request):
    if request.method == 'GET':
        code = start_repair_email(request.data['email'])
        return JsonResponse(code)


@api_view(['POST'])
@permission_classes((AllowAny,))
def confirm_reparation(request):
    if request.method == 'POST':
        code = repair_confirm(request.data['operationId'], request.data['code'])
        return JsonResponse({
            'code': code,
        })


@api_view(['POST'])
@permission_classes((AllowAny,))
def set_new_pass_reparation(request):
    if request.method == 'POST':
        code = set_new_password(request.data['operationId'], request.data['newPassword'])
        return JsonResponse({
            'code': code,
        })


@api_view(['GET'])
@permission_classes((AllowAny,))
def search(request):
    if request.method == 'GET':

        from_location = [float(x) for x in request.query_params['from'].split(',')]
        to_location = [float(x) for x in request.query_params['to'].split(',')]
        search_date = parser.parse(request.query_params['date_time'])
        constant = get_search_constant()

        # check if stops is near
        if is_locations_near(from_location, to_location, constant):
            err = notfar()
            return error(status=err['status'], header=err['header'], message=err['message'])

        # executing sql
        route_ids = get_route_ids_from_db(search_date, from_location, to_location, constant)

        if route_ids is None:
            # => sql не выполнился
            err = server_error()
            return error(status=err['status'], header=err['header'], message=err['message'])

        routes_objects = Route.objects.filter(id__in=route_ids).select_related('driver', 'driver__car', 'driver__city') \
            .prefetch_related('stops', 'stops__place')

        answer = generate_response(routes_objects, search_date, constant, from_location, to_location, request)

        return Response(answer)


class RouteViewSet(viewsets.ModelViewSet):
    queryset = Route.objects.all().order_by('date_time_start')
    serializer_class = RouteSerializer

    lookup_field = 'id'

    permission_classes = (IsAuthenticated,)

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            pass
        return HttpResponse(status=status.HTTP_200_OK)

    @action(methods=['post'], detail=True)
    def set_schedule(self, request, id=None):
        route = self.get_object()
        schedule = request.data['schedule']
        if len(schedule) != 7 and len(schedule) != 0:
            err = noschedule()
            return error(status=err['status'], header=err['header'], message=err['message'])
        for char in schedule:
            if not (char == '+' or char == '-'):
                err = noschedule()
                return error(status=err['status'], header=err['header'], message=err['message'])
        route.schedule = schedule

        if 'date_time_start' in request.data.keys() and 'date_time_finish' in request.data.keys():
            if parse(request.data['date_time_start']) > parse(request.data['date_time_finish']):
                err = invalid_schedule()
                return error(status=err['status'], header=err['header'], message=err['message'])

        if 'date_time_start' in request.data.keys():
            new_date = request.data['date_time_start']
            route.date_time_start = new_date

        if 'date_time_finish' in request.data.keys():
            new_date = request.data['date_time_finish']
            route.date_time_end = new_date

        route.save()
        return HttpResponse(status=200)

    @action(methods=['post'], detail=True)
    def delete(self, request, id=None):
        route = self.get_object()
        if route.driver.id != request.user.id:
            err = nocheats()
            return error(status=err['status'], header=err['header'], message=err['message'])
        if route.is_deleted:
            return error(status=404, header='deleted', message='Маршрут уже был удален')
        else:
            route.is_deleted = True
            route.save()
        return HttpResponse(status=200)


class StopViewSet(viewsets.ModelViewSet):
    queryset = Stop.objects.all().order_by('route', 'order')
    serializer_class = StopSerializer

    permission_classes = (IsAdminOrSelf,)


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_counter(request):
    user_count = int(get_pseudo_counter())
    user_count += User.objects.all().count()
    return JsonResponse({
        "count": user_count
    }, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_site_info(request):
    return JsonResponse(status=status.HTTP_200_OK, data=get_site_data())


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_sms_balance(request):
    s = SMSC()
    s.get_balance()
    return HttpResponse(status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def do_dangerous(request):
    tempo = [1, 2, 3, 4]
    for i in range(0, 20):
        tempo[i] *= 1
    return HttpResponse(status=status.HTTP_200_OK)
