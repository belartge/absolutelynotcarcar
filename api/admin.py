from datetime import datetime

from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from import_export.resources import ModelResource
from import_export.admin import ImportExportMixin, ImportMixin, ExportActionModelAdmin, ExportMixin

from .models import *

from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin

admin.site.register(City)
admin.site.register(PhoneCode)


class CarInline(admin.StackedInline):
    model = Car
    extra = 0
    fields = ['car_brend', 'car_number']


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('surname', 'name', 'patronym', 'email', 'birth_date', 'phone', 'city')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('surname', 'name', 'patronym', 'email', 'phone', 'city', 'is_active', 'is_admin')


class IsActiveSubscriptionFilter(admin.SimpleListFilter):
    title = 'Подписка активна'
    parameter_name = 'is_active_subscription'

    def lookups(self, request, model_admin):
        return (
            ('Yes', 'Да'),
            ('No', 'Нет'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'Yes':
            return queryset.filter(subscribed_until__gt=datetime.today())
        elif value == 'No':
            return queryset.exclude(subscribed_until__gt=datetime.today())
        return queryset


class UserAdmin(ExportMixin, BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm
    resource_class = UserResources

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('surname', 'name', 'patronym', 'email', 'birth_date', 'phone', 'is_admin', 'confirmed_user')
    list_filter = ('is_admin', 'gender', IsActiveSubscriptionFilter)

    inlines = [CarInline]

    fieldsets = (
        ('Персональные данные', {'fields': ('surname', 'name', 'patronym', 'gender', 'city', 'birth_date')}),
        ('О пользователе', {'fields': ('about',)}),
        ('Аватарка', {'fields': ('photo',)}),
        ('Контактные данные', {'fields': ('email', 'phone',)}),

        ('Permissions', {'fields': ('is_admin', 'confirmed_user', 'subscribed_until')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('surname', 'name', 'patronym', 'email', 'phone', 'birth_date', 'password1', 'password2')}
         ),
    )
    search_fields = ('email', 'phone', 'name')
    ordering = ('surname', 'email',)
    filter_horizontal = ()

    def is_active_subscription(self, obj):
        return obj.subscribed_until > datetime.today()

    is_active_subscription.boolean = True


# Now register the new UserAdmin...
admin.site.register(User, UserAdmin)
# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)


class StopModelInline(SortableInlineAdminMixin, admin.TabularInline):  # or admin.StackedInline
    model = Stop


class RouteAdmin(admin.ModelAdmin):
    inlines = (StopModelInline,)
    model = Route
    list_filter = ('is_deleted', 'date_time_start')
    list_display = ('__str__', 'date_time_start', 'is_deleted')


admin.site.register(Route, RouteAdmin)

# @admin.register(MyModel)

admin.site.register(Stop)


class PlaceAdmin(admin.ModelAdmin):
    model = Place
    list_display = ('short_name',  'full_name',  'lat', 'lng')


admin.site.register(Place, PlaceAdmin)
admin.site.register(PhoneChangingCode)

# admin.site.register(Route)
