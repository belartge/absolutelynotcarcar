from enum import Enum

from django.core.exceptions import ValidationError
from django.db import models
import jwt
from datetime import datetime, timedelta, date
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin
)
from django.utils.html import format_html
from easy_thumbnails.fields import ThumbnailerImageField
from easy_thumbnails.files import get_thumbnailer
from import_export import resources

from CarCarCity.settings.local_settings import SECRET_KEY
from model_utils import Choices


class Route(models.Model):
    driver = models.ForeignKey(
        'User',
        on_delete=models.CASCADE,
        verbose_name="Водитель"
    )

    date_time_start = models.DateTimeField(
        verbose_name='Время начала',
        default=datetime.today

    )
    date_time_end = models.DateTimeField(
        verbose_name='Время конца',
        default=datetime.today
    )

    is_backward = models.BooleanField(
        default=False,
        verbose_name="Обратный путь"
    )

    schedule = models.CharField(
        default="+++++++",
        max_length=7,
        blank=True,
        verbose_name="Расписание"
    )

    is_deleted = models.BooleanField(
        default=False,
        verbose_name="Удален"
    )

    def __str__(self):
        driva = self.driver.name + " " + self.driver.surname
        return str(self.id) + ". маршрут с водителем " + driva + " (" + str(self.driver.id) + ")"

    class Meta:
        verbose_name = 'маршрут'
        verbose_name_plural = 'маршруты'


class Stop(models.Model):
    place = models.ForeignKey(
        'Place',
        on_delete=models.CASCADE,
        verbose_name="Место"
    )

    route = models.ForeignKey(
        'Route',
        on_delete=models.CASCADE,
        verbose_name="Маршрут",
        related_name='stops'
    )

    minutes = models.PositiveSmallIntegerField(
        verbose_name='Кол-во минут от предыдущей остановки',
        default=5
    )

    order = models.PositiveIntegerField(
        default=0,
        blank=False,
        null=False
    )

    def __str__(self):
        return self.place.short_name

    class Meta:
        verbose_name = 'остановка'
        verbose_name_plural = 'остановки'
        ordering = ['order']


class City(models.Model):
    short_name = models.CharField(max_length=100, blank=False)
    full_name = models.CharField(max_length=300, primary_key=True)
    lat = models.FloatField()
    lng = models.FloatField()

    class Meta:
        ordering = ('short_name',)
        verbose_name = 'город'
        verbose_name_plural = 'города'

    def __str__(self):
        return self.full_name


class Place(models.Model):
    short_name = models.CharField(max_length=200, blank=False)
    full_name = models.TextField(blank=False)
    lat = models.FloatField()
    lng = models.FloatField()
    google_place_id = models.CharField(max_length=300, blank=False, primary_key=True)

    class Meta:
        ordering = ('short_name',)


class UserManager(BaseUserManager):
    """
    Django requires that custom users define their own Manager class. By
    inheriting from `BaseUserManager`, we get a lot of the same code used by
    Django to create a `User`.

    All we have to do is override the `create_user` function which we will use
    to create `User` objects.
    """

    def create_user(self, name, surname, email, phone, password=None, patronym="", birth_date=None, city=None,
                    about=None, gender='мужской'):

        """Create and return a `User` with an email, username and password."""
        if name is None:
            raise TypeError('Users must have a name.')

        if surname is None:
            raise TypeError('Users must have a surname.')

        if phone is None:
            raise TypeError('Users must have a phone.')

        if email is None:
            raise TypeError('Users must have an email address.')

        user = self.model(
            name=name,
            surname=surname,
            patronym=patronym,
            phone=phone,
            email=self.normalize_email(email),
            birth_date=birth_date,
            city=city,
            about=about,
            gender=gender
        )
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, name, surname, email, phone, password, patronym="", birth_date=None, gender='мужской'):
        """
        Create and return a `User` with superuser (admin) permissions.
        """
        if password is None:
            raise TypeError('Superusers must have a password.')

        user = self.create_user(name=name,
                                surname=surname,
                                patronym=patronym,
                                phone=phone,
                                email=email,
                                password=password,
                                birth_date=birth_date,
                                gender=gender)
        user.is_superuser = True
        user.is_admin = True
        user.confirmed_user = True
        user.save()

        return user


def birthday_validator(value):
    if value > date.today():
        raise ValidationError(
            _('%(value)s is more than now'),
            params={'value': value},
        )


class User(AbstractBaseUser, PermissionsMixin):
    surname = models.CharField(
        max_length=40,
        blank=False,
        verbose_name="Фамилия")
    name = models.CharField(
        max_length=30,
        blank=False,
        verbose_name="Имя")
    patronym = models.CharField(
        max_length=40,
        blank=True,
        default="",
        verbose_name="Отчество")
    birth_date = models.DateField(
        auto_now=False,
        verbose_name="День рождения",
        default="1971-01-01",
        validators=[birthday_validator]
    )
    email = models.EmailField(
        max_length=100,
        blank=False,
        unique=True,
        verbose_name="Адрес электронной почты")
    phone = models.CharField(
        max_length=20,
        blank=False,
        unique=True,
        verbose_name="Телефон")
    gender = models.CharField(
        max_length=7,
        default='мужской',
        verbose_name="Пол"

    )
    password = models.CharField(
        max_length=100,
        blank=False,
        verbose_name="Пароль")
    city = models.ForeignKey(
        City,
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
        verbose_name="Город")

    subscribed_until = models.DateTimeField(
        verbose_name="Подписка активна до",
        default="1970-01-01"
    )

    confirmed_user = models.BooleanField(
        verbose_name="Подтвержденный пользователь",
        default=False
    )

    photo = ThumbnailerImageField(
        upload_to='images/avatars',
        verbose_name='Ссылка фото',
        null=True,
        blank=True
    )

    about = models.TextField(
        verbose_name='О пользователе',
        max_length=500,
        null=True,
        blank=True,
        default=""
    )

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['surname', 'name', 'email', 'birth_date']
    #    city = models.ForeignKey(City, blank=False)

    objects = UserManager()

    def get_cropped_image(self):
        if self.photo:
            return get_thumbnailer(self.photo).get_thumbnail({'size': (150, 150), 'crop': True}).url
        else:
            return None

    def image_img(self):
        if self.photo:
            return format_html('<a href="{}" target="_blank"><img src="{}" width="270px"/></a>',
                               get_thumbnailer(self.photo).url,
                               get_thumbnailer(self.photo).get_thumbnail({'size': (270, 270), 'crop': True}).url)
        else:
            return '(Нет изображения)'

    image_img.short_description = 'Фото'
    image_img.allow_tags = True

    def __str__(self):
        """
        Returns a string representation of this `User`.

        This string is used when a `User` is printed in the console.
        """
        return '{}. {} {}{}'.format(self.id, self.name, self.surname, '(админ)' if self.is_admin else '')

    @property
    def token(self):
        """
        Allows us to get a user's token by calling `user.token` instead of
        `user.generate_jwt_token().

        The `@property` decorator above makes this possible. `token` is called
        a "dynamic property".
        """
        return self._generate_jwt_token()

    def get_full_name(self):
        """
        This method is required by Django for things like handling emails.
        Typically this would be the user's first and last name. Since we do
        not store the user's real name, we return their username instead.
        """
        if self.patronym is not None:
            return self.surname + " " + self.name + " " + self.patronym
        else:
            return self.surname + " " + self.name

    def get_short_name(self):
        """
        This method is required by Django for things like handling emails.
        Typically, this would be the user's first name. Since we do not store
        the user's real name, we return their username instead.
        """
        return self.name

    def _generate_jwt_token(self):
        """
        Generates a JSON Web Token that stores this user's ID and has an expiry
        date set to 60 days into the future.
        """
        dt = datetime.now() + timedelta(days=60)

        token = jwt.encode({
            'id': self.pk,
            'exp': int(dt.strftime('%s'))
        }, SECRET_KEY, algorithm='HS256')

        return token.decode('utf-8')

    @property
    def is_staff(self):
        """Is the user a member of staff?"""
        # Simplest possible answer: All admins are staff
        return self.is_admin

    class Meta:
        ordering = ('surname', 'name', 'patronym',)
        verbose_name_plural = "пользователи"
        verbose_name = "пользователь"


class UserResources(resources.ModelResource):
    class Meta:
        model = User
        fields = ('name', 'surname', 'email', 'phone', 'gender')


class Car(models.Model):
    car_brend = models.CharField(max_length=30, default="", verbose_name='Марка')
    car_number = models.CharField(max_length=15, default="", verbose_name='Гос. номер')
    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, default="", verbose_name='Машина')

    class Meta:
        verbose_name_plural = "машины"
        verbose_name = "машина"


class PhoneCode(models.Model):
    phone = models.CharField(max_length=20)
    code = models.CharField(max_length=10)


class PhoneChangingCode(models.Model):
    phone = models.CharField(max_length=20)
    new_phone = models.CharField(max_length=20)
    code = models.CharField(max_length=10)


class SubscriptionCode(models.Model):
    phone = models.CharField(max_length=20)
    months = models.IntegerField(default=0)


class RepairEmailCode(models.Model):
    email = models.CharField(max_length=100)
    code = models.CharField(max_length=10)
    operationId = models.CharField(max_length=20)
    confirmed = models.BooleanField(default=False)


class RepairPhoneCode(models.Model):
    # Todo возможно, стоит совместить с RepairEmailCode
    phone = models.CharField(max_length=20)
    code = models.CharField(max_length=10)
    operationId = models.CharField(max_length=20)
    confirmed = models.BooleanField(default=False)
