from rest_framework import routers

from .views import NewsViewSet

router = routers.DefaultRouter()
router.register(r'news', NewsViewSet)

app_name = 'news'
urlpatterns = router.urls
