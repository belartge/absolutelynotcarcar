from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_404_NOT_FOUND
from django.utils import timezone
from django.shortcuts import get_object_or_404

from .models import News
from .serializers import NewsSerializer


class NewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by('-pk')
    serializer_class = NewsSerializer

    def list(self, request, *args, **kwargs):

        queryset = News.objects.filter(date_publication__lte=timezone.now()).order_by('-date_publication')
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = News.objects.all()
        news_item = get_object_or_404(queryset, pk=pk)

        if news_item.date_publication <= timezone.now():
            serializer = self.get_serializer(news_item)
            return Response(serializer.data)
        else:
            return Response(data={'code': 404}, status=HTTP_404_NOT_FOUND)

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action in ['list', 'retrieve']:
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]
