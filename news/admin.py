from django.contrib import admin
from .models import News


class NewsAdmin(admin.ModelAdmin):
    date_hierarchy = 'date_publication'
    list_filter = ['show_publication_date']
    list_display = ('header', 'date_publication', 'show_publication_date', 'was_published')
    search_fields = ['header', 'body']


admin.site.register(News, NewsAdmin)
