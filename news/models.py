from django.db import models
from django.utils import timezone


class News(models.Model):

    header = models.CharField(
        max_length=200,
        verbose_name='Название'
    )

    body = models.TextField(
        verbose_name='Содержание новости'
    )

    date_publication = models.DateTimeField(
        default=timezone.now,
        verbose_name='Дата публикации',
    )

    show_publication_date = models.BooleanField(
        default=True,
        verbose_name='Показывать дату публикации?'
    )

    # была ли опубликована новость
    def was_published(self):
        if self.date_publication is None:
            return False
        else:
            return self.date_publication <= timezone.now()

    was_published.admin_order_field = 'published_date'
    was_published.boolean = True
    was_published.short_description = 'Опубликована?'

    def __str__(self):
        return self.header

    class Meta:
        verbose_name = 'Новости'
        verbose_name_plural = 'Новости'
        ordering = ['-date_publication']
