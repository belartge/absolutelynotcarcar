from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^cities/', views.cities, name='cities'),
    url(r'^places/', views.places, name='places'),
    url(r'^detail/', views.detail, name='detail')
]