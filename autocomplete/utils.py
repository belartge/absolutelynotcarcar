from jsondb.db import Database
import ujson
import logging
from typing import Dict, Any

from utils.debug_log_funcs import RunTimer, STRING_WITHOUT_API_FOR_LOG
from utils.local_db import get_db_name, get_db
from utils.google_places.api import autocomplete, autocomplete_by_location, detail
from utils.google_places.minimize_results import minimize_google_detail, minimize_google_predictions
from utils.str_funcs import remove_extra_spaces


# сделать
# todo сделать проверку приходящих от гугла значений по полю status и пробрасывать нужные ошибки


def get_db_name_for_predictions(city: str, types: str) -> str:
    if city:
        return get_db_name(city, 'predictions')
    else:
        if types:
            return get_db_name('_cities')
        else:
            return get_db_name('_without_city', 'predictions')


def get_db_name_for_predictions_location(city_location: str, city: str) -> str:
    if city:
        return get_db_name(city, 'location_predictions')
    else:
        return get_db_name(city_location, 'location_predictions')


def get_and_or_set_predictions_by_location(input_string, session_token, city_location, city):
    db_name = get_db_name_for_predictions_location(city_location, city)
    db = get_db(db_name)

    timer = RunTimer('JSON_DB-- доступ к db(' + db_name + ')')
    result_db = db.data(key=input_string)
    timer.stop_and_log()
    
    if result_db is None:

        timer = RunTimer('API-- доступ к api(location: ' + city_location + ')')
        diction = autocomplete_by_location(session_token, input_string, city_location)
        timer.stop_and_log()

        if diction['status'] in ['OK', 'ZERO_RESULTS']:
            diction = minimize_google_predictions(diction)
            db.data(key=input_string, value=ujson.dumps(diction, ensure_ascii=False))
        else:
            raise Exception('error getting api query')
    else:
        logging.debug(STRING_WITHOUT_API_FOR_LOG)
        diction = ujson.loads(result_db)
    return diction


def get_and_or_set_predictions(input_string, session_token, city='', types=''):
    db_name = get_db_name_for_predictions(city, types)
    input_string = remove_extra_spaces(city + ' ' + input_string)
    db = get_db(db_name)

    timer = RunTimer('JSON_DB-- доступ к db(' + db_name + ')')
    result_db = db.data(key=input_string)
    timer.stop_and_log()

    if result_db is None:

        timer = RunTimer('API-- доступ к api(input_string: ' + input_string + ', types: ' + types + ')')
        diction = autocomplete(session_token, input_string, types=types)
        timer.stop_and_log()

        if diction['status'] in ['OK', 'ZERO_RESULTS']:
            diction = minimize_google_predictions(diction)
            db.data(key=input_string, value=ujson.dumps(diction, ensure_ascii=False))
        else:
            raise Exception('error getting api query')
    else:
        logging.debug(STRING_WITHOUT_API_FOR_LOG)
        diction = ujson.loads(result_db)

    return diction


def get_and_or_set_detail(place_id, session_token):
    db_name = get_db_name('_detail')
    db = get_db(db_name)

    timer = RunTimer('JSON_DB-- доступ к db(' + db_name + ')')
    result_db = db.data(key=place_id)
    timer.stop_and_log()

    if result_db is None:

        timer = RunTimer('API-- доступ к api(place_id: ' + place_id + ')')
        diction = detail(session_token, place_id)
        timer.stop_and_log()

        if diction['status'] in ['OK', 'ZERO_RESULTS']:
            diction = minimize_google_detail(diction)
            diction['place_id'] = place_id
            db.data(key=place_id, value=ujson.dumps(diction, ensure_ascii=False))
        else:
            raise Exception('error getting api query')
    else:
        logging.debug(STRING_WITHOUT_API_FOR_LOG)
        diction = ujson.loads(result_db)
        diction['place_id'] = place_id

    return diction



