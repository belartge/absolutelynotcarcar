from django.http import JsonResponse
from rest_framework import status
from .utils import *
from utils.str_funcs import remove_extra_spaces
import logging


def cities(request):

    # GET параметры
    input_param = request.GET.get('input')
    session_param = request.GET.get('session')

    # проверка на валидность данных
    if input_param is None or session_param is None:
        return JsonResponse(data={'error_message': 'Не хватает параметров'}, status=status.HTTP_400_BAD_REQUEST)

    # вычисляемые параметры
    input_string = remove_extra_spaces(input_param).lower()
    session_token = session_param

    # получение результата из кэша или API
    try:
        diction = get_and_or_set_predictions(input_string, session_token, types='cities')
        return JsonResponse(diction, safe=False)
    except Exception as e:
        logging.error('error autocomplete: {}'.format(e))
        return JsonResponse(data={'error_message': 'Ошибка сервера'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def places(request):

    # GET параметры
    city_param = request.GET.get('city')
    city_location_param = request.GET.get('city_location')
    input_param = request.GET.get('input')
    session_param = request.GET.get('session')

    # проверка на валидность данных
    if input_param is None or session_param is None:
        return JsonResponse(data={'error_message': 'Не хватает параметров'}, status=status.HTTP_400_BAD_REQUEST)

    # вычисляемые параметры
    input_string = remove_extra_spaces(input_param).lower()
    session_token = session_param
    city = city_param.lower() if city_param else city_param
    city_location = city_location_param

    # получение результата из кэша или API
    try:
        if city_location:
            diction = get_and_or_set_predictions_by_location(input_string, session_token, city_location, city)
        else:
            diction = get_and_or_set_predictions(input_string, session_token, city=city)
        return JsonResponse(diction, safe=False)
    except Exception as e:
        logging.error('error autocomplete: {}'.format(e))
        return JsonResponse(data={'error_message': 'Ошибка сервера'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def detail(request):

    # GET параметры
    place_param = request.GET.get('place_id')
    session_param = request.GET.get('session')

    # проверка на валидность данных
    if place_param is None or session_param is None:
        return JsonResponse(data={'error_message': 'Не хватает параметров'}, status=status.HTTP_400_BAD_REQUEST)

    # вычисляемые параметры
    place_id = place_param
    session_token = session_param

    # получение результата из API
    try:
        diction = get_and_or_set_detail(place_id, session_token)
        return JsonResponse(diction, safe=False)
    except Exception as e:
        logging.error('error autocomplete: {}'.format(e))
        return JsonResponse(data={'error_message': 'Ошибка сервера'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
